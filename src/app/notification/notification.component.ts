/**
 * composant concernant la notification de l'utilisateurs (messages et info sur les echanges)
 */
import { Component, OnInit } from '@angular/core';
import { UserService } from '../models/index';

@Component({
    selector: 'notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

    public notifications = [];

    constructor(private userService : UserService) { }

    ngOnInit() {
        this.getAllNotification();
    }

    getAllNotification(){
        this.userService.getMessage(0,1).subscribe(
            data => {
                console.log(data)
                this.notifications = data.messages;
            },
            error => {

            }
        );
    }

    refuse(notification:Object){
        this.userService.refuseEchange(notification["id_exchange"]).subscribe(
            data => {
                toastr.success("Demande d'échange refusée", '', {positionClass: "toast-bottom-right"})
                notification["statut"] = 1
            },
            error =>{
                toastr.error("Erreur", '', {positionClass: "toast-bottom-right"})
            }
        )
    }

    accept(notification:Object){
        this.userService.acceptEchange(notification["id_exchange"]).subscribe(
            data => {
                toastr.success("Demande d'échange acceptée", '', {positionClass: "toast-bottom-right"});
                notification["statut"] = 1
            },
            error =>{
                toastr.error("Erreur", '', {positionClass: "toast-bottom-right"})
            }
        )
    }
}