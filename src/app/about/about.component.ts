import { Component } from '@angular/core';
/**
 * Ce composant permet de decrire notre application.
 */
@Component({
  moduleId: module.id,
  selector: 'about',
  templateUrl: 'about.component.html',
  styleUrls: ['about.component.css']
})
export class AboutComponent {
  constructor(){
  }
  
}
