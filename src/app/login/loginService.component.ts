/**
 * Composant d'authentification de l'utilisateur.
 */
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import * as myglobals from "../globals"
import 'rxjs/add/operator/map'
import * as myGlobals from "../globals"
declare var $ : any;
@Injectable()
export class LoginService {
    constructor(private http: Http) { }
    /**
     * Cette fonction permet l'authentification de l'utilisateur
     * 
     * @param mail : mail de l'utilisateur
     * @param password  : mot de passe de l'utilisateur
     */
    login(mail: any, password: any) {
        return this.http.post(myglobals.url+'user/login', JSON.stringify({ mail:mail, password: password }))
            .map((response: Response) => {
                let user = response.json();
                console.log(user);
                if (user && user.token) {
                    //this.resetCurrentUser();
                    localStorage.setItem('currentUser', JSON.stringify(user));

                }
            });
    }
    /**
     * 
     * Cette fonction permet à l'utilisateur de confirmer son inscription avec un 
     * code qu'ila reçu par mail.
     * @param mail : mail de l'utilisateur
     * @param code : code de confirmation de l'utilisateur reçu par mail
     * @param password  :mot de passe de l'utilisateur
     */
    confirmSubscription(mail: any,code:any, password: any) {
        return this.http.post(myglobals.url+'user/confirmCode', JSON.stringify({ mail:mail,code :code, password: password }))
            .map((response: Response) => {
                let user = response.json();
                if (user && user.token) {
                   // this.resetCurrentUser();
                    localStorage.setItem('currentUser', JSON.stringify(user));

                }
            });
    }
    /**
     * déconnexion de l'utilisateur
     */
  
    logout() {
      localStorage.removeItem('currentUser');
    }
    /**
     * fonction permettant au systéme l'envoi du mot de passe par mail.
     * 
     * @param mail : le mail ou l'utilisateur peut reçevoir le mot de passe
     */
    forgotPass(mail: any) {
        return this.http.post(myGlobals.url+'user/recoverPassword',JSON.stringify({mail : mail})).map((response: Response) => response.json());     
    }


    public resetCurrentUser(){
        if(localStorage.getItem("currentUser")!==null)
            localStorage.removeItem("currentUser");
    }


  
}