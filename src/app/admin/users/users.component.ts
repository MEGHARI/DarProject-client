/**
 * Ce composant permet de consulter tous les utilisateurs 
 * existants dans la base de donné de l'application.
 */
import { Component, OnInit ,ViewContainerRef} from '@angular/core';
import { Location } from '@angular/common';
import { Router,ActivatedRoute } from '@angular/router';
import {UserService} from "../../models/userService"
import {GameService} from "../../models/gameService"
import {User} from "../../models/user"
import { FormBuilder,FormControl,FormGroup,Validators } from "@angular/forms";


declare var $:any;
@Component({
    selector: 'users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})
export class UsersAdminComponent implements OnInit {
   
    public formGroupMessage: FormGroup;
    user : User;
    users = [];
    idAdmin : number;
    constructor(private activatedRoute: ActivatedRoute,private router : Router,
        public formBuilder : FormBuilder,private userService :UserService,
    private gameService :GameService) {
       if(this.router.url.match("/admin/[0-9]+/users")){
            this.activatedRoute.params.subscribe(params => {
                this.idAdmin= +params["idAdmin"]
                this.getAllUsers();
            });

       }

      }
    

    ngOnInit() { 
        
        $('[data-toggle="tooltip"]').tooltip();
        this.formGroupMessage = this.formBuilder.group({
            textMessage :['',Validators.required]
        })
    }
    /**
     * Cette fonction permet de récuperer la liste des utilisateurs.
     */

    getAllUsers(){
        
        this.userService.getAll().subscribe(
            
            data => {
                this.users = [];
                if(data["users"]!=undefined) {
                    data["users"].forEach(element => {
                        this.users.push(new User(element["first_name"],element["last_name"],
                        element["address"],element["mail"],element["id"],element["statut"],element["url_picture"],element["token"]))
                        })
                       console.log(data)
                }
               
            },
            error => { this.users=[]
            });
    }
    /**
     * Cette fonction permet à l'administrateur de banir un utilisateur,
     * et non pas le supprimer de la base de donnée,
     * 
     * 
     * @param us :l'utilisateur à bannir
     */

    bann(us:User){
        this.user = us;
        $('#modalBanned').on('show.bs.modal', function(e) {
            $('#modalBanned .modal-header #myModalLabel').html(us.lastName);
            $('#modalBanned .modal-body').html("Voulez vous vraiment bannir"+
            "<b>  "+us.lastName+"  "+us.firstName+" </b> à votre liste de jeux possédés ?");
            
        });
        
    }
    /**
     * Cette fonction permet à l'admin de remettre l'utilisateur,
     * après que ce dernier été bannis.
     * 
     * @param us 
     */
    unbann(us:User){
        this.user = us;
        $('#modalBanned').on('show.bs.modal', function(e) {
            $('#modalBanned .modal-header #myModalLabel').html(us.lastName);
            $('#modalBanned .modal-body').html("Voulez vous vraiment remétre"+
            "<b>  "+us.lastName+"  "+us.firstName+" </b> ?");
        });
    }
    /**
     * Cette fonction permet la confirmation de bannir ou remtere l'utilisateur.
     */
    confirm(){
        if(this.user.status==1){
            console.log(true)
            this.userService.bannUser(this.user.id).subscribe(
                data => {
                    console.log(data)
                    for(let us of this.users){
                        if(us.id == this.user.id){
                            us.status = -1;
                            console.log(us.id)
                            this.user.status=-1;
                            $("#modalBanned").modal("hide");
                            toastr.success(this.user.firstName+' bannis', '', {positionClass: "toast-bottom-right"});
                        }
                    }
                   
                    
                    
                },
                error => {
                    $("#modalBanned").modal("hide");
                    toastr.error(error.json()["message"]+" ", '') 
                }
            )
        }else if(this.user.status==-1){
            this.userService.unbannUser(this.user.id).subscribe(    
                data => {
                    console.log(data)
                    for(let us of this.users){
                        if(us.id == this.user.id){
                            us.status = 1;
                            this.user.status=1;
                            $("#modalBanned").modal("hide");
                            toastr.success(this.user.firstName+' remis', '', {positionClass: "toast-bottom-right"});
                            
                        }
                    }

                   
               
                },
                error => {
                    $("#modalBanned").modal("hide");
                    toastr.error(error.json()["message"]+" ", '') 
                   
                }
            )
        }
    }
    /**
     * Cette fonction permet à l'administrateur d'envoyer un message à un utilisateur.
     * @param us : l'utilisateur corréspondant
     */
    sendMessage(us:User){
        this.user = us;   
    }
    send(infoMessage : any){
        this.userService.sendMessageToUser(this.user.id,infoMessage.textMessage).subscribe(
            data => {
                $("#modalMessage").modal("hide");
                this.formGroupMessage.setValue({textMessage:""})
                toastr.success('message envoyé', '', {positionClass: "toast-bottom-right"});
            },
            error => {
                $("#modalMessage").modal("hide");
                toastr.error(error.json()["message"]+" ", '') 
                
            }

        )
    }

}
