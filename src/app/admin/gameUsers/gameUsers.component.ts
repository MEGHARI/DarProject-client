/**
 * Permet de lister à l'administrateur tous les utilisateurs du jeux selectioné
 */
import { Component, OnInit ,ViewContainerRef} from '@angular/core';
import { Location } from '@angular/common';
import { Router,ActivatedRoute } from '@angular/router';
import {UserService} from "../../models/userService"
import {GameService} from "../../models/gameService"
import {User} from "../../models/user"
import { FormBuilder,FormControl,FormGroup,Validators } from "@angular/forms";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

declare var $:any;
@Component({
    selector: 'users',
    templateUrl: './gameUsers.component.html',
    styleUrls: ['./gameUsers.component.css']  
})
export class GameUsersComponent implements OnInit {
   
    public formGroupMessage: FormGroup;
    user : User;
    users = [];
    idAdmin : number;
    constructor(private activatedRoute: ActivatedRoute,private router : Router,
        public formBuilder : FormBuilder,private userService :UserService,
    private gameService :GameService) {

       if(this.router.url.match("/admin/[0-9]+/games/[0-9]+/users")){
        this.activatedRoute.params.subscribe(params => {
            let idGame = +params["idGame"];
            this.idAdmin = +params["idAdmin"]
            this.getUsersByGame(idGame);
     
         });
       }

      }
    

    ngOnInit() { 
        
        $('[data-toggle="tooltip"]').tooltip();
        this.formGroupMessage = this.formBuilder.group({
            textMessage :['',Validators.required]
        })
    }
   
    /**
     * Cette fonction permet de bannir l'utilisateur 
     * 
     * @param us :l'utilisateur à bannir
     */
    bann(us:User){
        this.user = us;
        $('#modalBanned').on('show.bs.modal', function(e) {
            $('#modalBanned .modal-header #myModalLabel').html(us.lastName);
            $('#modalBanned .modal-body').html("Voulez vous vraiment bannir"+
            "<b>  "+us.lastName+"  "+us.firstName+" </b> à votre liste de jeux possédés ?");
            
        });
        
    }
    /**
     * Cette fonction permt de remetre l'utilisateur dèja bannis
     * 
     * @param us : l'utilisateur à remmetre 
     */
    unbann(us:User){
        this.user = us;
        $('#modalBanned').on('show.bs.modal', function(e) {
            $('#modalBanned .modal-header #myModalLabel').html(us.lastName);
            $('#modalBanned .modal-body').html("Voulez vous vraiment remétre"+
            "<b>  "+us.lastName+"  "+us.firstName+" </b> ?");
        });
    }
    /**
     * cette fonction permet de confirmer (bannir ou remetre) l'utilisateur selectioné
     * 
     */
    confirm(){
        console.log(this.user.id)
        if(this.user.status==1){
            console.log(true)
            this.userService.bannUser(this.user.id).subscribe(
                data => {
                    console.log(data)
                    for(let us of this.users){
                        if(us.id == this.user.id){
                            us.status = -1;
                            console.log(us.id)
                            this.user.status=-1;
                            $("#modalBanned").modal("hide");
                            toastr.success('utilisateur bannis', '', {positionClass: "toast-bottom-center"});
                        }
                    }
                   
                    
                    
                },
                error => {
                    $("#modalBanned").modal("hide");
                 
                    toastr.error(error.json()["error"]["message"], '') 
                }
            )
        }else if(this.user.status==-1){
            this.userService.unbannUser(this.user.id).subscribe(    
                data => {
                    console.log(data)
                    for(let us of this.users){
                        if(us.id == this.user.id){
                            us.status = 1;
                            this.user.status=1;
                            $("#modalBanned").modal("hide");
                            toastr.success('utilisateur remis', '', {positionClass: "toast-bottom-center"});
                            
                        }
                    }

                   
               
                },
                error => {
                    $("#modalBanned").modal("hide");
                    toastr.error(error.json()["error"]["message"]+" ", '') 
                   
                }
            )
        }
    }
    /**
     * Cette fonction permet à l'administrateur d'envoyer un message à l'utilisateur
     * 
     * @param us :l'utilisateur correspondant
     */
    sendMessage(us:User){
        this.user = us;   
    }
    send(infoMessage : any){
        this.userService.sendMessageToUser(this.user.id,infoMessage.textMessage).subscribe(
            data => {
                $("#modalMessage").modal("hide");
                this.formGroupMessage.setValue({textMessage:""})
                toastr.success('message envoyé', '', {positionClass: "toast-bottom-center"});
            },
            error => {
                $("#modalMessage").modal("hide");
                toastr.error(error.json()["message"]+" ", '') 
                
            }

        )
    }
    /**
     * Permet à l'administreur de récuperer tous les utilisateurs possédant le jeux 
     * 
     * @param id :L'id du jeux 
     */

    getUsersByGame(id : number){
        this.userService.getUsersByGame(id).subscribe(
            data => {
                this.users= [];
                console.log(data)
                if(data["users_of_game"]!=undefined) {
                    data["users_of_game"].forEach(element => {
                        this.users.push(new User(element["first_name"],element["last_name"],
                        element["address"],element["mail"],element["id"],element["statut"],element['url_picture'],element["token"]))
                            
                    })
                } },
            error => {this.users=[];}
        )
    }
   
}
