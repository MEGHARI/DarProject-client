/**
 * la navbar de plateform admin,
 * elle permet de notifier l'admin en ce qui concerne les nouveaux messages
 */
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {User} from "../../models/user"
import {Router} from "@angular/router"
import {UserService} from "../../models/userService"
import {GameService} from "../../models/gameService"
import { CompleterService, CompleterData } from 'ng2-completer';
import * as myGlobals from "../../globals";
declare var $:any;

@Component({
    selector: 'navbar-admin',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarAdminComponent implements OnInit, OnDestroy {
    protected dataService: CompleterData;
    protected searchData = [];
    protected searchStr: string;
    public  logged : boolean;
    public admin : User;
    public nbNotifications:number;
    public nbMessages:number;
    protected intervale;

    constructor(private completerService: CompleterService,
        public router :Router,
        private userService : UserService,
        private gameService : GameService)
        {
            this.intervale = setInterval(() => {this.refreshNumberNotifications();},1000*19)
        }

    ngOnInit() {
        this.getCacheNotifications();
        this.setUser()
     }
     /**
      * permet à l'admin de se déconnecter ,destruction du token 
      */

    logOut(){
         
         this.userService.logout().subscribe(
             data =>{
                 console.log("success")
                 console.log(data)
             },
             error =>{console.log(error.json())}
         )
        localStorage.removeItem("currentUser")
        this.logged=false;
        this.router.navigate(["/home"]) 
        
    }/**
     * Permet de récupérer les donnée de l'admin du localstorage
     */
     setUser(){
        if(localStorage.getItem("currentUser") === null){
            this.logged = false;
        }else {
            let infUser = JSON.parse(localStorage.getItem("currentUser"))
            this.admin= new User(infUser["last_name"],infUser["first_name"],infUser["address"],infUser["mail"],infUser["id"],infUser["statut"],infUser["url_picture"],infUser["token"])
            this.logged = true;
        }
       
    } 
    /**
     * Permet de rafraichir les notifications reçu du server,
     * notifications + messages
     */
    refreshData(){
        if(this.searchStr.length>3){
            console.log("entré")
            this.gameService.autoSearchGames(this.searchStr).subscribe(
            data => {
                this.searchData = [];
                if(data["games"]){
                    data["games"].forEach(element => {
                        this.searchData.push(element["name"])
                    });
                }
                this.dataService = this.completerService.local(this.searchData, null, null);
            },
            error => {
                console.log(error.json())
                //this.searchData = [];
            });
        }
    }
    /**
     * Permet de rechercher le jeux à échanger
     * 
     * @param title : le nom du jeux à chercher
     */
    searchForExchanging(title : string){
        this.router.navigate(["/users/"+this.admin.id+"/suggestions/"+title]);
       
    }

    refreshNumberNotifications(){
        this.userService.getNumberNotifications().subscribe(
            data => {
                console.log(data);
                myGlobals.setNbNotifications(data.notifications_count["system_count"]);
                myGlobals.setNbMessage(data.notifications_count["message_count"]);
                this.nbNotifications = myGlobals.nbNotifications
                this.nbMessages = myGlobals.nbMessages
            },
            error =>{
                console.log(error.json());
            }
        )
    }
    /**
     * Permet de detruire le composant 
     */

    ngOnDestroy() {
        if (this.intervale) {
          clearInterval(this.intervale);
        }
    }
    /**
     * récuerer le cache de notificaation entant valeur global
     */
    getCacheNotifications(){
        this.nbMessages = myGlobals.nbMessages;
        this.nbNotifications = myGlobals.nbNotifications;
    }
}