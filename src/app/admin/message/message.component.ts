/**
 * Représante la prtie messagerie de l'aplication ,
 * permet à l'utilisateur donné de recevoir et envoyer des messages aux 
 * autres utilisateurs,et administrateur de l'application
 */
import { Component, OnInit, OnDestroy } from '@angular/core';
import {User, UserService} from "../../models/index"
import { Router, ActivatedRoute ,NavigationEnd} from '@angular/router';
@Component({
    selector: 'messages',
    templateUrl: './message.component.html',
    styleUrls: ['./message.component.css']
})
export class MessagesAdminComponent implements OnInit, OnDestroy {
    public user : User;
    public idReceiver : number;
    public messageText : string;
    public page :number;
    public messages = [];
    public conversations = [];
    protected intervale;

    constructor(private userService:UserService,private router :Router) {
             this.intervale = setInterval(() => {this.refreshMessage(this.idReceiver);},1000*5)
    }
    ngOnInit() { 
        this.setUser();
        this.getConversations();
        console.log(this.conversations.length);
    }
    /**
     * Permet de récuperer les données de l'administrateur.
     */
    setUser(){
        let infUser = JSON.parse(localStorage.getItem("currentUser"))
        this.user= new User(infUser["last_name"],infUser["first_name"],infUser["address"],infUser["mail"],infUser["id"],infUser["statut"],infUser["url_picture"],infUser["token"])
    }
    /**
     * Récuperer la conversation avec l'utilisateur 
     * 
     * @param id : l'id de l'utilisateur
     */

    getMessages(id:number){
        this.page = 1;
        this.messages = [];
        this.idReceiver = id;
        this.userService.getMessage(id,this.page).subscribe(
            data =>{
                console.log(data)
                data.messages.reverse();
                data.messages.forEach(e => {
                    this.messages.push(e)
                });
            },
            error =>{
                console.log();
            }
        )
    }
    /**
     * permet de mettre la conversation avec l'utilisateur
     * 
     * @param id l'id de l'utilisateur
     */

    refreshMessage(id:number){
        //if(this.router.url.match("/users/[0-9]+/messages")){
        this.idReceiver = id;
        this.userService.getMessage(id,1).subscribe(
            data =>{
                console.log(data)
                data.messages.reverse();
                data.messages.forEach(e => {
                    let find = false;
                    this.messages.forEach(e2=>{
                        if(e2["_id"] == e["_id"]){
                            find=true;
                        }
                    })
                    if(!find)
                        this.messages.push(e)
                });
            },
            error =>{
                console.log();
            }
        )
    //}
    }

    /**
     * permet de récuperer toute la conversation de tous les utilisateurs
     */

    getConversations(){
        this.userService.getConversations().subscribe(
            data => {
                console.log(data)
                data["conversations"].forEach(e => {
                    if(e["_id"] != 0){
                        this.conversations.push({firstName : e["firstName"],
                    lastName: e["lastName"], id: e["_id"], count: e["count"],
                urlPicture: e["url_picture"]});
                    }
                });
                
                if(this.conversations.length != 0)
                    this.getMessages(this.conversations[0]["id"])
            },
            error => {
                console.log(error.json())
            }
        )
    }
    /**
     * 
     */
    sendMessage(){
        if(this.messageText.length>0){
            this.userService.sendMessageToUser(this.idReceiver, this.messageText).subscribe(
                data => {
                    this.messages.push({
                        message : data["message"],
                        date: data["date"],
                        id_receiver : data["id_receiver"],
                        id_sender : data["id_sender"],
                        url_picture : this.user["urlPicture"],
                        _id : data["_id"],
                    })
                },
                error => {

                }
            )
        }
    }

    onScroll(){
        this.page++;
        this.userService.getMessage(this.idReceiver,this.page).subscribe(
            data =>{
                console.log(data)
                data.messages.forEach(e => {
                    this.messages.unshift(e)
                });
            },
            error =>{
                console.log();
            }
        )
    }

    ngOnDestroy() {
        if (this.intervale) {
          clearInterval(this.intervale);
        }
    }
}