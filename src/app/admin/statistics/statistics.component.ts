import { Component, OnInit } from '@angular/core';
import {UserService} from "../../models/userService"
import {Game} from "../../models/game"
import { Router,ActivatedRoute } from '@angular/router';
declare var $:any;

@Component({
    selector: 'statistics',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.css']
})
export class StaticticsComponent implements OnInit {
    idAdmin : number;
    idUser : number;
    statistics = [];

    
    constructor(private userService :UserService,
        private router:Router,private activatedRoute : ActivatedRoute) { 
        if(this.router.url.match("/admin/[0-9]+/users/[0-9]+/statistics")){
             this.activatedRoute.params.subscribe(params => {
                 this.idAdmin = +params['idAdmin']
                 this.idUser = +params['idUser']
                 this.getStatsByUser(this.idUser)
                 console.log(this.statistics.length)
              });
            }
    }

    ngOnInit() { }
    getStatsByUser(id : number){   
        this.userService.getStatisticsByUser(id).subscribe(
            
            data =>{data["exchanges"].forEach(element => {
                console.log(data)
                this.statistics.push(element);
            });
        },
            error => {console.log(error)}
        )
    }
}