/**
 * Composant permettant à l'administrateur de récuperer la liste des jeux d'un utilisateur donnée,
 * pour les consulter et verifier leurs informations
 */
import { Component, OnInit } from '@angular/core';
import {UserService} from "../../models/userService"
import {GameService} from "../../models/gameService"
import {Game} from "../../models/game"
import { Router,ActivatedRoute } from '@angular/router';
declare var $:any;
@Component({  
    selector: 'games',
    templateUrl: './userGames.component.html',
    styleUrls: ['./userGames.component.css']
})
export class UserGamesComponent implements OnInit {
    game :Game;
    idGameSelected:number =0;
    games = [];
    idAdmin : number;
    idUser : number;
    constructor(private gameService : GameService,private userService :UserService,
        private router:Router,private activatedRoute : ActivatedRoute) { 
        if(this.router.url.match("/admin/[0-9]+/users/[0-9]+/games")){
             this.activatedRoute.params.subscribe(params => {
                 this.idAdmin = +params['idAdmin']
                 this.idUser = +params["idUser"];
                 this.getGamesByUser(this.idUser);
              });
            }
    }
    ngOnInit() {}
    /**
     * Permet d'avoire les inforations du jeux
     * @param ga  : le jeux
     * 
     */
    detail(ga :Game){
        this.game  = ga;
        this.idGameSelected = ga.id;
        console.log(ga)
        console.log(this.game)
            $('#product_view').on('show.bs.modal', function(e) {
                $('.modal-title').html(ga.title);
                $('#image').attr("src","http:"+ga.image);
                $('#summary').html(ga.overview);
                $('#users').html("liste d'utilisateurs");
               // $('#users').attr("href","admin/"+JSON.parse(localStorage.getItem("currentUser"))["id"]+"/games/"+ga.id+"/users")
             });
    }
    /**
     * Cacher les détails du jeu donné
     */
    hidenModal(){
        $( "a" ).click(function( event ) {
            event.preventDefault();
          });
        $("#product_view").modal("hide");
    }
    /**
     * Cette fonction permet de lister les jeux de l'utilisateur concérné
     * 
     * @param id : l'identifiant de 'utilisateur concérné
     */
    getGamesByUser(id : number){

        this.gameService.getGamesByUser(id).subscribe(
            data => {

                this.games=[];
                console.log(data["games"])
                if(data["games"]!==undefined){
                    data["games"].forEach(element => {
                        this.games.push(new Game(element["id"],element["game_name"],element["summary"],element["date_release"],
                    element["platform"],element["game_cover"]))
    
                    });
                   
                }
            },
            error =>{this.games=[]}
        )
    }
}
