/**
 * Permet à l'administrateur  de consulter les jeux enregistrer dans la base de donnée 
 *
 */
import { Component, OnInit } from '@angular/core';
import {UserService} from "../../models/userService"
import {GameService} from "../../models/gameService"
import {Game} from "../../models/game"
import { Router,ActivatedRoute } from '@angular/router';
declare var $:any;
@Component({  
    selector: 'games',
    templateUrl: './games.component.html',
    styleUrls: ['./games.component.css']
})
export class GamesAdminComponent implements OnInit {
    public  idGame :number;
    games = [];
    idAdmin: number;
    constructor(private gameService : GameService,private userService :UserService,
        private router:Router,private activatedRoute : ActivatedRoute) { 
            this.idAdmin = JSON.parse(localStorage.getItem("currentUser"))["id"];
            console.log(this.idAdmin)
        }
   
    ngOnInit() {this.listGames() ;}
    /**
     * Permet de lister toutes la liste des jeux qui esistent das la base de donnée
     */
    listGames(){
        this.gameService.getAllGames().subscribe(
            data => {
                this.games=[];
                console.log(data["games"])
                if(data["games"]==undefined){
                   //nothing to do
                } else {
                    data["games"].forEach(element => {
                        this.games.push(new Game(element["id"],element["game_name"],element["summary"],element["date_release"],
                    element["platform_name"],element["cover"]))
                    console.log(element)
                    });
                }
            },
            error =>{this.games=[]}
        )
    }
    /**
     * 
     * @param ga :"le jeux selectionné par l'administrateur pour voir plus de détails"
     */
    detail(ga :Game){
        this.idGame = ga.id; 
            $('#product_view').on('show.bs.modal', function(e) {
                $('.modal-title').html(ga.title);
                $('#image').attr("src","http:"+ga.image);
                $('#summary').html(ga.overview);
                $('#users').html("liste d'utilisateurs");
             });
    }
    /**
     * permet de cacher les détails du jeux selectioné
     */

    hidenMoal(){
        $( "a" ).click(function( event ) {
            event.preventDefault();
          });
        $("#product_view").modal("hide");
    }

}
